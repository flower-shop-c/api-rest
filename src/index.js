async function main() {
  console.log("main starts");
  const app = require('./app');
  await app.listen(app.get('port'));
  console.log(`server on port ${app.get('port')}`);
  // await run1().catch(console.dir);
}

main();