const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategoriesSchema = Schema({
  name: String,
  img:String,
  description:String
});

module.exports = mongoose.model('categories', CategoriesSchema);
