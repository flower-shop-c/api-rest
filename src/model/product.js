const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = Schema({
  name: String,
  price:Number,
  discount:Number,
  id_category:String,
  description:String,
  img:String,
  stock:Number
});

module.exports = mongoose.model('products', ProductSchema);
