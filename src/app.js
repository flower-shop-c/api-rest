const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');

const env = require( './config/db-connection');
const mongoose = require('mongoose');

const app = express();

mongoose.connect(env.uri,  { useNewUrlParser: true , useUnifiedTopology: true} )
//   .then(db => console.log('db connected'))
//   .catch(err => console.log(err));

const db = mongoose.connection;
db.on('error', function() {
    console.log('connection error:')
});
db.once('open', function() {
  // we're connected!
  console.log('db connected...')
});

// importing routes
const indexRoutes = require('./routes/index');
const userRoutes = require('./routes/users');
const authRoutes = require('./routes/auth');
const productsRoutes = require('./routes/products');
const categoriesRoutes = require('./routes/categories');


// settings
app.set('port', process.env.PORT || 3000);
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// routes
app.use('/api/', indexRoutes);
app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/products', productsRoutes);
app.use('/api/categories', categoriesRoutes);

console.log('app');
// starting the server
module.exports = app;
