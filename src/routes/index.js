const express = require('express');
const router = express.Router();
const User = require('../model/user');
const middleware = require('./../middlewares/middleware');

const auth = middleware.ensureAuthenticated;

router.get('/',  async (req, res) => {

  const users = await User.find();
  console.log(users)
  res.send({status:'Connectado'});
});


module.exports = router;
