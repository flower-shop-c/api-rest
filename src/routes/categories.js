const express = require('express');
const router = express.Router();
const Category = require('../model/category');
const middleware = require('../middlewares/middleware');

const auth = middleware.ensureAuthenticated;

router.get('/', auth,  async (req, res) => {
  const categories = await  Category.find();
  res.send(categories);
});
 
router.get('/:id', auth, async (req, res, next) => {
    const category = await Category.findById(req.params.id);
    console.log(req.params.id, category);
    res.send(category);
});

router.post('/', auth, async (req, res, next) => {
  console.log(req.body);
  const category = new Category(req.body);
  await category.save();
  res.send({status:'Guardado'});
});

router.put('/:id', auth, async (req, res, next) => {
    const category = await Category.findByIdAndUpdate(req.params.id, req.body);
    console.log(req.params.id, req.body, category);    
    await category.save();
    res.send({status:'updated'});
  });

router.delete('/:id', auth, async (req, res, next) => {
  await Category.findByIdAndDelete(req.params.id);  
  
  res.send({status:'deleted'});
});




// router.post('/edit/:id', auth, async (req, res, next) => {
//   const { id } = req.params;
//   await Product.update({_id: id}, req.body);
//   res.redirect('/');
// });

// router.get('/delete/:id', auth, async (req, res, next) => {
//   let { id } = req.params;
//   await User.remove({_id: id});
//   res.redirect('/');
// });


module.exports = router;
