const express = require('express');
const router = express.Router();
const User = require('../model/user');
var middleware = require('../middlewares/middleware');

const auth = middleware.ensureAuthenticated;

router.get('/',  async (req, res) => {
  const users = await User.find();
  res.send(users);
});

router.get('/:id', auth, async (req, res, next) => {
    const user = await User.findById(req.params.id);
    console.log(req.params.id, user);
    res.send(user);
});

router.post('/', async (req, res, next) => {
  console.log(req.body);
  const user = new Product(req.body);
  await User.save();
  res.send({status:'Guardado'});
});

router.put('/:id', auth, async (req, res, next) => {
    const user = await User.findByIdAndUpdate(req.params.id, req.body);
    console.log(req.params.id, req.body, user);    
    await User.save();
    res.send({status:'updated'});
  });

router.delete('/:id', auth, async (req, res, next) => {
  let { id } = req.params;
  const user = await User.findByIdAndDelete(id);  
  await user.save();
  res.send({status:'deleted'});
});
module.exports = router;
