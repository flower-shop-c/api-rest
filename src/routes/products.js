const express = require('express');
const router = express.Router();
const Product = require('../model/product');
const middleware = require('./../middlewares/middleware');

const auth = middleware.ensureAuthenticated;

router.get('/', auth,  async (req, res) => {
  const products = await Product.find();
  res.send(products);
});

router.get('/:id', auth, async (req, res, next) => {
    const product = await Product.findById(req.params.id);
    console.log(req.params.id, product);
    res.send(product);
});

router.post('/', auth, async (req, res, next) => {
  console.log(req.body);
  const product = new Product(req.body);
  await product.save();
  res.send({status:'Guardado'});
});

router.put('/:id', auth, async (req, res, next) => {
    const product = await Product.findByIdAndUpdate(req.params.id, req.body);
    console.log(req.params.id, req.body, product);    
    await product.save();
    res.send({status:'updated'});
  });

router.delete('/:id', auth, async (req, res, next) => {
  await Product.findByIdAndDelete(req.params.id);  
  
  res.send({status:'deleted'});
});




// router.post('/edit/:id', auth, async (req, res, next) => {
//   const { id } = req.params;
//   await Product.update({_id: id}, req.body);
//   res.redirect('/');
// });

// router.get('/delete/:id', auth, async (req, res, next) => {
//   let { id } = req.params;
//   await User.remove({_id: id});
//   res.redirect('/');
// });


module.exports = router;
